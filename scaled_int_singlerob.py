








from numpy import *


def scaled_int_singlerob(scale, delt, delt1):

	scale_ddot = (scale**2-1)/(2*delt)

	delt_sc = 2*delt1/(scale+1)

	time_grid = linspace(0, delt_sc, delt1/0.01)

	newdelt = time_grid[1]-time_grid[0]

	scale_newgrid = zeros(len(time_grid))
	scale_newgrid[0] = 1.0

	for i in range(0, len(time_grid)-1):
		scale_newgrid[i+1] = scale_newgrid[i]+scale_ddot*newdelt





	return time_grid, scale_newgrid, scale_ddot, delt_sc



