




from numpy import *

from sc_fn_compute_singlerob import sc_fn_compute_singlerob


def scale_compute(a_scale, b_scale, c_scale, vel_rob, theta_rob, thetadot_sol, Vmax, Vmin,  xddotmax, yddotmax, thetadotmax, sc_int, V_desired, delt):

	


	
	inner_iter = 3
	
	for k in range(0, inner_iter):
		counter = 0
		mat_tscc_left = zeros((100,1))

		mat_tscc_right = zeros((100,1))
		


		for i in range(0, len(a_scale)):

			if(a_scale[i][0]>0 and c_scale[i][0]>0 and b_scale[i][0]<0):\


				
				c1 = -(a_scale[i][0]+(b_scale[i][0]/(2*sc_int)))
				c2 = (b_scale[i][0]/2)*sc_int+c_scale[i][0]
				
				mat_tscc_left[counter][0] = c1
				mat_tscc_right[counter][0] = c2
				counter = counter+1


			

			if(a_scale[i][0]>=0 and c_scale[i][0]>=0 and b_scale[i][0]>=0):
				
				
				c1 = -1.0
				c2 = 200.00
				
				mat_tscc_left[counter][0] = c1
				mat_tscc_right[counter][0] = c2
				counter = counter+1


			if(a_scale[i][0]<0 and c_scale[i][0]>0 ):

				root1 = (-b_scale[i][0]+sqrt(b_scale[i][0]**2 - 4*a_scale[i][0]*c_scale[i][0]))/(2*a_scale[i][0])

				root2 = (-b_scale[i][0]-sqrt(b_scale[i][0]**2 - 4*a_scale[i][0]*c_scale[i][0]))/(2*a_scale[i][0])
				
				scalemax = max(root1,root2)
				scalemin = min(root1,root2)
				
				if(scalemin>0):
				   mat_tscc_left[counter][0] = 1
				   mat_tscc_right[counter][0] = (scalemax**2) 
				   mat_tscc_left[counter+1][0] = -1
				   mat_tscc_right[counter+1][0] = -(scalemin**2)
				  
				   counter = counter+2
				   
				if(scalemin<0):
				   mat_tscc_left[counter][0] = 1
				   mat_tscc_right[counter][0] = (scalemax**2)
				   
				   counter = counter+1

			
			if(a_scale[i][0]<0 and c_scale[i][0]<0):

				root1 = (-b_scale[i][0]+sqrt(b_scale[i][0]**2 - 4*a_scale[i][0]*c_scale[i][0]))/(2*a_scale[i][0])

				root2 = (-b_scale[i][0]-sqrt(b_scale[i][0]**2 - 4*a_scale[i][0]*c_scale[i][0]))/(2*a_scale[i][0])
				
				scalemax = max(root1,root2)
				scalemin = min(root1,root2)
				
				if(scalemin>0):
				   mat_tscc_left[counter][0] = 1
				   mat_tscc_right[counter][0] = (scalemax**2) 
				   mat_tscc_left[counter+1][0] = -1
				   mat_tscc_right[counter+1][0] = -(scalemin**2)
				  
				   counter = counter+2
				   
				if(scalemin<0):
				   mat_tscc_left[counter][0] = 1
				   mat_tscc_right[counter][0] = (scalemax**2)
				   
				   counter = counter+1
	   


			if(a_scale[i][0]>0 and c_scale[i][0]<0):


				root1 = (-b_scale[i][0]+sqrt(b_scale[i][0]**2 - 4*a_scale[i][0]*c_scale[i][0]))/(2*c_scale[i][0])

				root2 = (-b_scale[i][0]-sqrt(b_scale[i][0]**2 - 4*a_scale[i][0]*c_scale[i][0]))/(2*c_scale[i][0])
				
				scalemax = max(root1,root2)
				scalemin = min(root1,root2)

				
				if(scalemin>0):
				   mat_tscc_left[counter][0] = 1
				   mat_tscc_right[counter][0] = (1/scalemin**2)
				   mat_tscc_left[counter+1][0] = -1
				   mat_tscc_right[counter+1][0] = -(1/scalemax**2)
				   
				   counter = counter+2
				   
				if(scalemin<0):
				   mat_tscc_left[counter][0] = -1
				   mat_tscc_right[counter][0] = -(1/scalemax**2)
				   
				   counter = counter+1   
				   
				

		weight_slack = 10000
		mat_tscc_left = mat_tscc_left[~all(mat_tscc_left==0,axis=1)]
		mat_tscc_right = mat_tscc_right[~all(mat_tscc_right==0,axis=1)]
		

		



		
		scale = sc_fn_compute_singlerob(mat_tscc_left, mat_tscc_right, vel_rob, theta_rob, thetadot_sol, Vmax, Vmin, xddotmax, yddotmax, thetadotmax, weight_slack, V_desired, delt)
		sc_int = scale
		
		

	return scale	       



	







		




