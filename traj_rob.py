





from numpy import *


def traj_rob(x_rob, y_rob, vel_rob, theta_rob, thetadot_rob, time_grid, scale_newgrid, scale_ddot):



	xt_traj = zeros(len(time_grid))
	yt_traj = zeros(len(time_grid))
	vel_traj = zeros(len(time_grid))
	theta_traj = zeros(len(time_grid))
	thetadot_traj = zeros(len(time_grid))

	xt_traj[0] = x_rob
	yt_traj[0] = y_rob

	vel_traj[0] = vel_rob

	theta_traj[0] = theta_rob

	thetadot_traj[0] = thetadot_rob

	newdelt = time_grid[1]-time_grid[0]

	

	for i in range(0, len(time_grid)-1):

		xt_traj[i+1] = xt_traj[i]+vel_traj[i]*cos(theta_traj[i]+thetadot_traj[i]*newdelt)*newdelt

		yt_traj[i+1] = yt_traj[i]+vel_traj[i]*sin(theta_traj[i]+thetadot_traj[i]*newdelt)*newdelt

		theta_traj[i+1] = theta_traj[i]+thetadot_traj[i]*newdelt

		
		thetadot_traj[i+1] = thetadot_rob*scale_newgrid[i+1]

		vel_traj[i+1] = vel_rob*scale_newgrid[i+1]

		


	return xt_traj, yt_traj, vel_traj, theta_traj, thetadot_traj	




	
	
	
	


	
	
	