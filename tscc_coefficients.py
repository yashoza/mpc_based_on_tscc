





from numpy import *
from coefficients import coefficients


def tscc_coefficients(next_state_obst, x_rob, y_rob, vel_rob, theta_rob, thetadot_rob, thetadotmax, rad, delt, num, count1):

	a = zeros((num,1))
	b = zeros((num,1))
	c = zeros((num,1))

	for i in range(0, num):

		a[i][0], b[i][0], c[i][0] = coefficients(next_state_obst[i][0], next_state_obst[i][1], next_state_obst[i][2], next_state_obst[i][3], x_rob, y_rob, vel_rob, theta_rob, thetadot_rob, rad, delt)


	
	return a, b, c	



