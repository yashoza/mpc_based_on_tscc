




from numpy import *
from cvxopt import solvers
import cvxopt
import scipy.linalg
import time


def sc_fn_compute_singlerob(mat_tscc_left, mat_tscc_right,vel_rob, theta_rob, thetadot_sol, Vmax, Vmin, xddotmax, yddotmax, thetadotmax, weight_slack, V_desired, delt):






	s_max = Vmax/vel_rob
	s_min = Vmin/vel_rob
	s_desired = V_desired/vel_rob
	s_max_theta = thetadotmax/(abs(thetadot_sol)+0.0000001)

	xddot = -vel_rob*sin(theta_rob)*thetadot_sol
	yddot = vel_rob*cos(theta_rob)*thetadot_sol
	xdot = vel_rob*cos(theta_rob)
	ydot = vel_rob*sin(theta_rob)

	u = delt

	conx_ub = (xdot/(2*u))
	conx_lb = -(xdot/(2*u))
	   
	cony_ub = (ydot/(2*u))
	cony_lb = -(ydot/(2*u)) 
	


	A_ineq1 = mat_tscc_left


	A_ineq1 = hstack(( mat_tscc_left, -identity(len(mat_tscc_left)) ))

	A_ineq2 = -identity(len(mat_tscc_left)+1)

	A_ineq3 = zeros(( 1, len(mat_tscc_left)+1 ))

	A_ineq3[0][0] = 1.0

	A_ineq_accx_ub = hstack(( conx_ub*ones((1,1)), zeros((1, len(mat_tscc_left)  )) ))

	A_ineq_accx_lb = hstack(( conx_lb*ones((1,1)), zeros((1, len(mat_tscc_left)  )) ))

	A_ineq_accy_ub = hstack(( cony_ub*ones((1,1)), zeros((1, len(mat_tscc_left)  )) ))

	A_ineq_accy_lb = hstack(( conx_lb*ones((1,1)), zeros((1, len(mat_tscc_left)  )) ))


	A_ineq_vmin = zeros(( 1, len(mat_tscc_left)+1 ))

	A_ineq_vmin[0][0] = -1.0

	A_ineq_thetadot = zeros(( 1, len(mat_tscc_left)+1 ))

	A_ineq_thetadot[0][0] = 1.0


	A_ineq4 = vstack((A_ineq_accx_ub, A_ineq_accx_lb, A_ineq_accy_ub, A_ineq_accy_lb, A_ineq_vmin, A_ineq_thetadot  ))




	B_ineq1 = mat_tscc_right

	B_ineq2 = zeros(( len(mat_tscc_left)+1, 1 ))

	B_ineq3 = array([[s_max**2]])

	vstack((xddotmax+(xdot/(2*u))-xddot,  xddotmax-(xdot/(2*u))+xddot, yddotmax+(ydot/(2*u))-yddot, yddotmax-(ydot/(2*u))+yddot))

	B_ineq_accx_ub = (xddotmax+(xdot/(2*u))-xddot)*ones((1,1))
	B_ineq_accx_lb = (xddotmax-(xdot/(2*u))+xddot)*ones((1,1))
	B_ineq_accy_ub = (yddotmax+(ydot/(2*u))-yddot)*ones((1,1))
	B_ineq_accy_lb = (yddotmax-(ydot/(2*u))+yddot)*ones((1,1))
	B_ineq_vmin = -(s_min**2)*ones((1,1))
	B_ineq_thetadot = (s_max_theta**2)*ones((1,1))

	B_ineq4 = vstack((B_ineq_accx_ub, B_ineq_accx_lb, B_ineq_accy_ub, B_ineq_accy_lb, B_ineq_vmin, B_ineq_thetadot  ))





	A_ineq = vstack(( A_ineq1, A_ineq2, A_ineq3, A_ineq4 ))

	B_ineq = vstack(( B_ineq1, B_ineq2, B_ineq3, B_ineq4 ))

	scale_desired = array([[s_desired**2]])

	mat_scale = zeros((1, len(mat_tscc_left)+1))
	mat_scale[0][0] = 1.0

	cost_scale = dot(transpose(mat_scale), mat_scale)

	lincost_scale = -dot(transpose(mat_scale), scale_desired)

	lincost_slack = ones(( len(mat_tscc_left)+1,1 ))
	lincost_slack[0][0] = 0.0

	cost = cost_scale
	lincost = lincost_scale+weight_slack*lincost_slack


	opts = {'feastol':0.01, 'show_progress': False}


	sol = solvers.qp(cvxopt.matrix(cost, tc='d'), cvxopt.matrix(lincost, tc='d'), cvxopt.matrix(A_ineq, tc='d'), cvxopt.matrix(B_ineq, tc='d'), None, None, options = opts)

	

	sol1 = sol['x'] 


	sol2 = array(sol1)

	scale = sqrt(sol2[0][0])

	# maxviol = max(sol2[1:len(sol2)])

	# print maxviol, scale

	

	return scale






	
