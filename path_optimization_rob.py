




from numpy import *
from cvxopt import solvers
import cvxopt
from linearized_collcone import linearized_collcone

import time





def path_optimization_rob(next_state_obst, x_rob, y_rob, vel_rob, theta_rob, thetadot_rob, xf_rob, yf_rob, thetadotmax, Rmax, rad, delt, weight_slack, num, count1,prev_dist,cur_dist,prev_omega):

	bazuka = array([])

	collcone_const = zeros(num)
	gradient_collcone = zeros(num)


	for i in range(0, num):

		collcone_const[i], gradient_collcone[i] = linearized_collcone(next_state_obst[i][0], next_state_obst[i][1], next_state_obst[i][2], next_state_obst[i][3], x_rob, y_rob, vel_rob, theta_rob, thetadot_rob, rad, delt)


	gradient_collcone = transpose(array([gradient_collcone]))
	collcone_const = transpose(array([collcone_const]))
	



	num_const = shape(collcone_const)[0]	

	A_ineq_collcone = hstack(( -gradient_collcone*ones((num_const, 1)), -identity(num_const)  ))
	
	A_ineq_Rmax_ub = hstack(( ones((1, 1)), zeros((1, num_const))  ))

	A_ineq_Rmax_lb = hstack(( -ones((1, 1)), zeros((1, num_const))  )) 

	A_ineq_theta_ub = hstack(( ones((1, 1)), zeros((1, num_const))  ))

	A_ineq_theta_lb = hstack(( -ones((1, 1)), zeros((1, num_const))  ))

	
	A_ineq_slack = hstack(( zeros((num_const, 1)), -identity(num_const)  ))


	B_ineq_collcone = collcone_const - gradient_collcone*thetadot_rob

	B_ineq_Rmax_ub = vel_rob/Rmax
	B_ineq_Rmax_lb = vel_rob/Rmax

	B_ineq_theta_ub = thetadotmax

	B_ineq_theta_lb = thetadotmax





	B_ineq_slack = zeros((num_const, 1))

	
	A_ineq = vstack(( A_ineq_collcone,  A_ineq_Rmax_ub, A_ineq_Rmax_lb, A_ineq_theta_ub, A_ineq_theta_lb, A_ineq_slack ))

	B_ineq = vstack(( B_ineq_collcone,  B_ineq_Rmax_ub, B_ineq_Rmax_lb, B_ineq_theta_ub, B_ineq_theta_lb, B_ineq_slack ))
	

	# print(x_rob, y_rob)

	theta_desired = arctan2(yf_rob-y_rob, xf_rob-x_rob)

	
	A_cost_theta = hstack(( delt*ones((1,1)), zeros((1, num_const)) ))
	B_cost_theta = array([[theta_desired-theta_rob]])

	cost_theta = dot(transpose(A_cost_theta), A_cost_theta)
	
	lincost_theta = -dot(transpose(A_cost_theta), B_cost_theta)

	lincost_slack = ones((num_const+1, 1))
	lincost_slack[0][0] = 0

	cost_smoothness = 5.0

	rem_cost_smooth_A = hstack(( ones((1,1)), zeros((1, num_const)) ))
	rem_cost_smooth_B = array([[prev_omega]])


	cost_theta = cost_theta + cost_smoothness*(dot(transpose(rem_cost_smooth_A), rem_cost_smooth_A))
	lincost_theta = lincost_theta -cost_smoothness*(dot(transpose(rem_cost_smooth_A), rem_cost_smooth_B))

	# if(count1>1500):
	# 	weight_slack = 0.0

	for r in range(0,num):
		if cur_dist[r] > prev_dist[r] :
			weight_slack[r+1][0] = 0
			bazuka = 1 

	cost = cost_theta

	lincost = weight_slack*lincost_slack+lincost_theta

	start = time.time()

	opts = {'feastol':0.01, 'show_progress': False}

	sol = solvers.qp(cvxopt.matrix(cost, tc='d'), cvxopt.matrix(lincost, tc='d'), cvxopt.matrix(A_ineq, tc='d'), cvxopt.matrix(B_ineq, tc='d'), None, None, options = opts)

	# print time.time()-start

	sol1 = sol['x'] 


	sol2 = array(sol1)

	thetadot_sol = sol2[0][0]

	maxviol = max(sol2[1:len(sol2)])

	# print thetadot_sol, maxviol

	
	return thetadot_sol, bazuka






	

















		




