





from numpy import *



def linearized_collcone(x_obst, y_obst, vel_obst, theta_obst, x_rob, y_rob, vel_rob, theta_rob, thetadot_rob, rad, delt):



	
	R = 2.5*rad
	x1t = x_rob+vel_rob*cos(theta_rob+thetadot_rob*delt)*delt
	y1t = y_rob+vel_rob*sin(theta_rob+thetadot_rob*delt)*delt
	xot = x_obst
	yot = y_obst
	x1tdot = vel_rob*cos(theta_rob+thetadot_rob*delt)     
	y1tdot = vel_rob*sin(theta_rob+thetadot_rob*delt)
	xotdot = vel_obst*cos(theta_obst)
	yotdot = vel_obst*sin(theta_obst)





	A = (-R**2*(x1tdot**2+y1tdot**2)+((x1t-xot)*y1tdot+x1tdot*(-y1t+yot))**2)

	B = 2*(-((x1t-xot)*y1tdot+x1tdot*(-y1t+yot))*(xotdot*(-y1t+yot)+(x1t-xot)*yotdot)+R**2*(x1tdot*xotdot+y1tdot*yotdot))

	C = (xotdot*(y1t-yot)+(-x1t+xot)*yotdot)**2-R**2*(xotdot**2+yotdot**2)



	collcone_const = A+B+C

	gradient_collcone = 2*R**2*(delt*vel_obst*vel_rob*sin(theta_obst)*cos(delt*thetadot_rob + theta_rob) - delt*vel_obst*vel_rob*sin(delt*thetadot_rob + theta_rob)*cos(theta_obst)) + 2*(vel_obst*(-delt*vel_rob*sin(delt*thetadot_rob + theta_rob) + y_obst - y_rob)*cos(theta_obst) + vel_obst*(delt*vel_rob*cos(delt*thetadot_rob + theta_rob) - x_obst + x_rob)*sin(theta_obst))*(delt**2*vel_rob**2*sin(delt*thetadot_rob + theta_rob)**2 + delt**2*vel_rob**2*cos(delt*thetadot_rob + theta_rob)**2 + delt*vel_rob*(-delt*vel_rob*sin(delt*thetadot_rob + theta_rob) + y_obst - y_rob)*sin(delt*thetadot_rob + theta_rob) - delt*vel_rob*(delt*vel_rob*cos(delt*thetadot_rob + theta_rob) - x_obst + x_rob)*cos(delt*thetadot_rob + theta_rob)) + (vel_obst*(delt*vel_rob*sin(delt*thetadot_rob + theta_rob) - y_obst + y_rob)*cos(theta_obst) + vel_obst*(-delt*vel_rob*cos(delt*thetadot_rob + theta_rob) + x_obst - x_rob)*sin(theta_obst))*(2*delt**2*vel_obst*vel_rob*sin(theta_obst)*sin(delt*thetadot_rob + theta_rob) + 2*delt**2*vel_obst*vel_rob*cos(theta_obst)*cos(delt*thetadot_rob + theta_rob)) + 2*(-vel_rob*(-delt*vel_rob*sin(delt*thetadot_rob + theta_rob) + y_obst - y_rob)*cos(delt*thetadot_rob + theta_rob) - vel_rob*(delt*vel_rob*cos(delt*thetadot_rob + theta_rob) - x_obst + x_rob)*sin(delt*thetadot_rob + theta_rob))*(-delt**2*vel_obst*vel_rob*sin(theta_obst)*sin(delt*thetadot_rob + theta_rob) - delt**2*vel_obst*vel_rob*cos(theta_obst)*cos(delt*thetadot_rob + theta_rob)) + (vel_rob*(-delt*vel_rob*sin(delt*thetadot_rob + theta_rob) + y_obst - y_rob)*cos(delt*thetadot_rob + theta_rob) + vel_rob*(delt*vel_rob*cos(delt*thetadot_rob + theta_rob) - x_obst + x_rob)*sin(delt*thetadot_rob + theta_rob))*(-2*delt**2*vel_rob**2*sin(delt*thetadot_rob + theta_rob)**2 - 2*delt**2*vel_rob**2*cos(delt*thetadot_rob + theta_rob)**2 - 2*delt*vel_rob*(-delt*vel_rob*sin(delt*thetadot_rob + theta_rob) + y_obst - y_rob)*sin(delt*thetadot_rob + theta_rob) + 2*delt*vel_rob*(delt*vel_rob*cos(delt*thetadot_rob + theta_rob) - x_obst + x_rob)*cos(delt*thetadot_rob + theta_rob))


	return collcone_const, gradient_collcone



