




from numpy import *


def traj_obst(time_grid, x_obst, y_obst, vel_obst, theta_obst):

	newdelt = time_grid[1]-time_grid[0]

	xt_obst = zeros(len(time_grid))

	yt_obst = zeros(len(time_grid))


	xt_obst[0] = x_obst
	yt_obst[0] = y_obst

	for i in range(0, len(time_grid)-1):
		xt_obst[i+1] = xt_obst[i]+vel_obst*cos(theta_obst)*newdelt
		yt_obst[i+1] = yt_obst[i]+vel_obst*sin(theta_obst)*newdelt


	return xt_obst, yt_obst
	
		
