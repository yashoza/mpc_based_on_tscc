





from numpy import *



def next_state_obst_compute(x, y, vel, theta, delt):


	x_obst = x+vel*cos(theta)*delt
	y_obst = y+vel*sin(theta)*delt

	return x_obst, y_obst, vel, theta



