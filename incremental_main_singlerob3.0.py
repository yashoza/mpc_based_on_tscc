
from numpy import *
from next_state_obst_compute import next_state_obst_compute
from path_optimization_rob import path_optimization_rob
from scaled_int_singlerob import scaled_int_singlerob
from traj_obst import traj_obst
from traj_rob import traj_rob
from tscc_coefficients import tscc_coefficients

from scale_compute import scale_compute
from lookahead_pointupdate import lookahead_pointupdate
import matplotlib.patches as patches


import matplotlib.pyplot as plt
import matplotlib as mpl
from drawnow import drawnow

import time








r = 140*2.5

r1 = 120


rad = 9.0/1.2

num = 5

Vmin = 3.0
Vmax = 12.0
Vinit = 8.0

V_desired = 8.0

V_obst = array([3.0, 3.0, 1.0, 3.1, 3.1])

xddotmax = 6
yddotmax = 6

Rmax = 20

thetadotmax = 0.8
thetaddotmax = 0.2





theta_1 = 0*pi/180


theta_rob = 0*pi/180

#########################################




theta = linspace(0,2*pi,1601)
def makeFig():
	base_x = 0
	base_y = 0
	# for h in range(horizon,iili,horizon):
	# 	for pop in range(0,num):
	# 		if pop == 1:
	# 			pop = 2
	# 		elif pop == 4:
	# 			break
	# 		km1 = x_current_obst[h-horizon, pop]
	# 		km2 = x_current_obst[h, pop]
	# 		km3 = y_current_obst[h-horizon, pop]
	# 		km4 = y_current_obst[h, pop]
	# 		if pop == 3:
	# 			km1 = km1-70
	# 			km2 = km2-70
	# 		elif pop == 2:
	# 			km1 = km1-10
	# 			km2 = km2-10
	# 		plt.plot([km1,km2],[km3,km4],'r',lw=1)

	# 	base_x = x_current_rob[h, 0]
	# 	base_y = y_current_rob[h, 0]
	# 	plt.plot([x_current_rob[h-horizon, 0],x_current_rob[h, 0]],[y_current_rob[h-horizon, 0],y_current_rob[h, 0]],'b',lw=1)

	for pop in range(0,num):
		if pop == 1:
			pop = 2
		elif pop == 3:
			break
		base_x = x_current_obst[iili-horizon, pop]
		base_y = y_current_obst[iili-horizon, pop]

		if pop == 3:
			base_x = base_x - 70
		elif pop == 2:
			base_x = base_x-10

		base_theta = theta_1
		base_length = 22
		base_width = 11
		rect = patches.Rectangle((base_x-0.5*base_length,base_y-0.5*base_width), base_length,base_width,
			fill = True,
			facecolor='r',edgecolor='k', linewidth=1)
		t = mpl.transforms.Affine2D().rotate_around(base_x, base_y,\
			base_theta)

		rect.set_transform(t + plt.gca().transData)

		plt.gca().add_patch(rect)

	base_x = x_current_rob[iili-horizon, 0]
	base_y = y_current_rob[iili-horizon, 0]
	base_theta = theta_current_rob[iili-horizon,0]
	base_length = 22
	base_width = 11
	rect = patches.Rectangle((base_x-0.5*base_length,base_y-0.5*base_width), base_length,base_width,
		fill = True,
		facecolor='b',edgecolor='k', linewidth=1)
	t = mpl.transforms.Affine2D().rotate_around(base_x, base_y,\
		base_theta)

	rect.set_transform(t + plt.gca().transData)

	plt.gca().add_patch(rect)

	base_x = -100
	plt.plot([xo_rob,base_x],[-31,-31],'k',lw = 1.3)
	plt.plot([xo_rob,base_x],[-11,-11],'k--',lw = 0.8)
	plt.plot([xo_rob,base_x],[9,9],'k',lw = 1.3)
	frame1 = plt.gca()
	frame1.axes.get_xaxis().set_visible(False)
	frame1.axes.get_yaxis().set_visible(False)
	# plt.plot([xo_rob-30,base_x+30],[-11,-11],'k--',lw = 1)
	# plt.axes().set_aspect( 0.4, 'datalim')
	plt.axis('equal')
	
	# axes = plt.gca()
	# axes.set_xlim([base_x-100,base_x+100])
	# axes.set_ylim([base_y-100,base_y+100])


		# base_x = x_current_rob[h, 0]
		# base_y = y_current_rob[h, 0]
		# base_theta = theta_current_rob[h,0]
		# base_length = 25
		# base_width = 13
		# rect = patches.Rectangle((base_x-0.5*base_length,base_y-0.5*base_width), base_length,base_width,
		# 	fill = True,
		# 	facecolor='b',edgecolor='k', linewidth=1)
		# t = mpl.transforms.Affine2D().rotate_around(base_x, base_y,\
		# 	base_theta)

		# rect.set_transform(t + plt.gca().transData)

		# plt.gca().add_patch(rect)



############################################################## obstacle1 initial position and velocity

###################obstacle  1

xo_1 = -605
yo_1 = -1.03

xdot_1 = V_obst[0]*cos(theta_1)
ydot_1 = V_obst[0]*sin(theta_1)



# ########################### obstacle 2 


xo_2 = -605.0
yo_2 = 20.1

xdot_2 = V_obst[1]*cos(theta_1)
ydot_2 = V_obst[1]*sin(theta_1)


# ############################################obstacle 3 


xo_3 = -425
yo_3 = -21.5

xdot_3 = V_obst[2]*cos(theta_1)
ydot_3 = V_obst[2]*sin(theta_1)


# #####################################################obstacle 4



xo_4 = -310
yo_4 = -75

xdot_4 = V_obst[3]*cos(theta_1)
ydot_4 = V_obst[3]*sin(theta_1)



# ############################################# obstacle 5 


xo_5 = -310
yo_5 = -75

xdot_5 = V_obst[4]*cos(theta_1)
ydot_5 = V_obst[4]*sin(theta_1)



########################################################## robot




xo_rob = -700
yo_rob = -1.5

xdot_rob = Vinit*cos(theta_rob)
ydot_rob = Vinit*sin(theta_rob)

xf_rob = -400
yf_rob = -21.5





##############################################################



count = 10730


init_x_obst = array([xo_1, xo_2, xo_3, xo_4, xo_5])
init_y_obst = array([yo_1, yo_2, yo_3, yo_4, yo_5])

theta_obst = array([theta_1, theta_1, theta_1, theta_1, theta_1])







init_vel = ones(num)

init_veldot = ones(num)

x_current_obst = zeros((count, num))
y_current_obst = zeros((count, num))


x_current_rob = zeros((count, 1))
y_current_rob = zeros((count, 1))
theta_current_rob = zeros((count, 1))


vel_current_rob = zeros((count, 1))
veldot_current_rob = zeros((count, 1))


thetadot_current_rob = zeros((count, 1))

x_current_rob[0][0] = xo_rob 
y_current_rob[0][0] = yo_rob
theta_current_rob[0][0] = theta_rob
vel_current_rob[0][0] = Vinit
veldot_current_rob[0][0] = 0.0
thetadot_current_rob[0][0] = 0.0

############################## putting initial values

for i in range(0, num):
	x_current_obst[0][i] = init_x_obst[i]
	y_current_obst[0][i] = init_y_obst[i]




count1 = 1

delt = 0.8

delt1 = 0.1
horizon = 1



next_state_rob = zeros((1,5))

next_state_obst = zeros((num, 4))


sc_int = arange(1, 3.5, 0.25)
sc_int = sorted(sc_int, reverse = True)

thetadot_guess = 0.0

scale_guess = 1.5

maxiter = 1070

rem_scale = zeros((maxiter,1))
rem_time = zeros((maxiter,1))

xf_rob1 = zeros(maxiter)
yf_rob1 = zeros(maxiter)

prev_dist = zeros((num,1))
cur_dist = zeros((num,1))

deviation = -1
timing = 0
blocky = 0
prev_omega = 0
malinga = 0


while (count1<maxiter*horizon):

	pathx_desired = array([xo_rob, xf_rob])
	pathy_desired = array([yo_rob, yf_rob])

	if timing <27.09:
		num = 2
	else:
		num = 3

	# if(timing > 45) and (malinga == 0):
	# 	y_current_obst[count1-1,2] = -475
	# 	y_current_obst[count1-1,2] = -75
	# 	malinga = 1


	xf_rob, yf_rob = lookahead_pointupdate(x_current_obst[count1-1][0], y_current_obst[count1-1][0], x_current_rob[count1-1][0], y_current_rob[count1-1][0], xf_rob, yf_rob, vel_current_rob[count1-1][0], theta_current_rob[count1-1][0] )

	
	weight_slack = 3000*ones((num+1, 1))


	for i in range(0, num):
		x_obst_nex, y_obst_nex, vel_obst_nex, theta_obst_nex = next_state_obst_compute(x_current_obst[count1-1,i], y_current_obst[count1-1, i], V_obst[i], theta_obst[i], delt )
		next_state_obst[i] = array([x_obst_nex, y_obst_nex, vel_obst_nex, theta_obst_nex])
		cur_dist[i] = sqrt((x_obst_nex - x_current_rob[count1-1,0])**2 + (y_obst_nex - y_current_rob[count1-1,0])**2)	

	for i in range(0, 3):	
		
		thetadot_sol, bazuka = path_optimization_rob(next_state_obst, x_current_rob[count1-1][0], y_current_rob[count1-1,0], vel_current_rob[count1-1, 0], theta_current_rob[count1-1, 0], thetadot_guess, xf_rob, yf_rob, thetadotmax, Rmax, rad, delt, weight_slack, num, count1,prev_dist,cur_dist,prev_omega)
		thetadot_guess = thetadot_sol

	prev_omega = thetadot_sol
	
	a_scale, b_scale, c_scale = tscc_coefficients(next_state_obst, x_current_rob[count1-1][0], y_current_rob[count1-1,0], vel_current_rob[count1-1, 0], theta_current_rob[count1-1, 0], thetadot_sol, thetadotmax, rad, delt, num, count1)
	
	# plt.plot(a_scale, '-r')
	# plt.plot(b_scale, '-b')
	# plt.plot(c_scale, '-k')
	# plt.show()


	scale1 = scale_compute(a_scale, b_scale, c_scale, vel_current_rob[count1-1, 0], theta_current_rob[count1-1, 0], thetadot_sol, Vmax, Vmin, xddotmax, yddotmax, thetadotmax, scale_guess, V_desired, delt)
 
	# print scale1, min(a_scale+b_scale+c_scale)

	scale_guess = scale1



	scale = scale1

	rem_scale[blocky,0] = scale1
	

	time_grid, scale_newgrid, scale_ddot, delt_sc = scaled_int_singlerob(scale, delt, delt1)


	for u in range(0,num):
		prev_dist[u] = cur_dist[u]

	horizon = len(time_grid)
	timing = timing + time_grid[9]
	print(timing)
	rem_time[blocky,0] = timing

	num = 5

	for i in range(0, num):


		xt_obst, yt_obst = traj_obst(time_grid, x_current_obst[count1-1, i], y_current_obst[count1-1, i], V_obst[i], theta_obst[i])

		x_current_obst[count1:count1+horizon, i] = xt_obst
				
		y_current_obst[count1:count1+horizon, i] = yt_obst

	x_rob_nex, y_rob_nex, vel_rob_nex, theta_rob_nex, thetadot_rob_nex = traj_rob(x_current_rob[count1-1,0], y_current_rob[count1-1,0], vel_current_rob[count1-1,0], theta_current_rob[count1-1,0], thetadot_sol, time_grid, scale_newgrid, scale_ddot)	



	x_current_rob[count1:count1+horizon, 0] = x_rob_nex
	y_current_rob[count1:count1+horizon, 0] = y_rob_nex
	vel_current_rob[count1:count1+horizon, 0] = vel_rob_nex
	theta_current_rob[count1:count1+horizon, 0] = theta_rob_nex
	thetadot_current_rob[count1:count1+horizon, 0] = thetadot_rob_nex


	if blocky % 4 == 0:
		
		x_rob = x_current_rob[i, 0]
		y_rob = y_current_rob[i, 0]
		thr_rob = theta_current_rob[i,0]
		iili = count1 + horizon
		drawnow(makeFig) 
		# plt.pause(0.00001)

		plt.savefig('/home/mithun/Desktop/figures_ml/pathe_' + str(iili) +'.eps', format='eps', dpi=10000)

	count1 = count1+horizon
	blocky = blocky + 1


	# if bazuka == 1:
	# 	print(timing)

# plt.plot(x_current_rob[0:count1,0], y_current_rob[0:count1,0])
# plt.plot(x_current_obst[0:count1,0], y_current_obst[0:count1,0])


# plt.plot(xf_rob, yf_rob, 'or')
# # plt.plot(theta_current_rob)

# plt.axis('equal')
# plt.show()









# for iili in xrange(0, maxiter*horizon, 40):
# 		x_1 = x_current_obst[i, 0]
# 		y_1 = y_current_obst[i, 0]

# 		x_1 = x_current_obst[i, 1]
# 		y_1 = y_current_obst[i, 1]
		
# 		x_rob = x_current_rob[i, 0]
# 		y_rob = y_current_rob[i, 0]
# 		thr_rob = theta_current_rob[i,0]

# 		# xf_rob = xf_rob1[i]
# 		# yf_rob = yf_rob1[i]
		
# 		drawnow(makeFig) 
# 		# plt.savefig('/home/mithun/Desktop/New/complicated_overtake_tscc/path_' + str(iili) +'.eps', format='eps', dpi=1000)
# 		plt.pause(0.005)      



# text_filex = open("/home/mithun/Desktop/x_cor_robo.txt", "w")
# for yo in range(0,count1):
# 	text_filex.write("%s \n" % x_current_rob[yo,0])
# text_filex.close()

# text_filey = open("/home/mithun/Desktop/y_cor_rob.txt", "w")
# for yo in range(0,count1):
# 	text_filey.write("%s \n" % y_current_rob[yo,0])
# text_filey.close()

# text_fileth = open("/home/mithun/Desktop/theta_cor_rob.txt", "w")
# for yo in range(0,count1):
# 	text_fileth.write("%s \n" % theta_current_rob[yo,0])
# text_fileth.close()

# text_filevel = open("/home/mithun/Desktop/vel_cor_rob.txt", "w")
# for yo in range(0,count1):
# 	text_filevel.write("%s \n" % vel_current_rob[yo,0])
# text_filevel.close()



plt.figure(6)
plt.plot(rem_scale[0:blocky-1,0])
plt.xlabel('Time [ms]', fontsize=18)
plt.ylabel('Scale [rad/s]', fontsize=18) 


plt.figure(3)
plt.plot(rem_time[:,0],vel_current_rob[0:count1-1:10,0],lw = 2)
plt.plot([rem_time[0,0],rem_time[-1,0]],[12,12],'r--',lw = 1.4)
plt.plot([rem_time[0,0]],[14],'r*',lw = 0.04)
plt.plot([rem_time[0,0],rem_time[-1,0]],[0,0],'r--',lw = 1.4)
plt.plot([rem_time[0,0]],[-1],'r*',lw = 0.04)
plt.ylabel('Velocity [m/s]', fontsize=15)
plt.axes().set_aspect( 1.0, 'box')
plt.locator_params(axis='y', nbins=5)
# plt.grid(True, lw = 0.1)
plt.savefig('/home/mithun/Desktop/a_velocity_overtaking.eps', format='eps',bbox_inches='tight', dpi=600)

print(rem_time[3:maxiter-1,0].shape)
print(thetadot_current_rob[3*horizon:count1-1:10,0].shape)
plt.figure(8)
plt.plot(rem_time[3:maxiter-1,0] ,thetadot_current_rob[4*horizon:count1-1:10,0],lw=2)
# plt.plot([rem_time[0,0],rem_time[-1,0]],[0.4,0.4],'r--',lw = 1.4)
plt.xlabel('Time [sec]', fontsize=15)
plt.ylabel('Omega [rad/s]', fontsize=15) 
plt.axes().set_aspect(75, 'box')
plt.savefig('/home/mithun/Desktop/a_omega_overtaking.eps', format='eps', bbox_inches='tight', dpi=600)




plt.show()